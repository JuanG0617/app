package juanpablogarcia_id000076980.conversor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText temp;
    private TextView Text1;
    private RadioButton C1,F1,K1,C2,F2,K2;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        temp = (EditText)findViewById(R.id.temp);
        Text1 = (TextView)findViewById(R.id.Text1);
        C1 = (RadioButton)findViewById(R.id.C1);
        F1 = (RadioButton)findViewById(R.id.F1);
        K1 = (RadioButton)findViewById(R.id.K1);
        C2 = (RadioButton)findViewById(R.id.C2);
        F2 = (RadioButton)findViewById(R.id.F2);
        K2 = (RadioButton)findViewById(R.id.K2);

    }

    public void operar(View view) {
        String temperatura=temp.getText().toString();
        int grados=Integer.parseInt(temperatura);
        if (C1.isChecked()==true)  (F2.isChecked()==true)  {
            double F= grados*(9/5)+32;
            String resultado=String.valueOf(F);
            Text1.setText(resultado);
        } else
        if (K1.isChecked()==true) {
            double K= grados + 273.15;
            String resultado=String.valueOf(K);
            Text1.setText(resultado);
        }
    }
    public void info(View view) {
        Intent i = new Intent(this, AcercaDe.class);
        startActivity(i);
    }

}
